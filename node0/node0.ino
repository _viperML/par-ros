// node0.ino
#include <ros.h>
#include <std_msgs/UInt16.h>

const auto SENSOR_PIN = 5;
const auto DELAY = 100;

// Tipo de mensaje del tópico
std_msgs::UInt16 light;

// Definición de publicador en tópico
ros::Publisher publisher("light", &light);

// Inicialización del nodo
ros::NodeHandle node_handle;
void setup() {
  node_handle.initNode();
  node_handle.advertise(publisher);
}

void loop() {
  // Lectura del sensor
  light.data = analogRead(SENSOR_PIN);

  // Publicación en ROS
  publisher.publish(&light);
  node_handle.spinOnce();

  delay(DELAY);
}
