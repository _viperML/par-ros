{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    ros-overlay = {
      url = "github:lopsided98/nix-ros-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    ros-overlay,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ros-overlay.overlays.default];
    };
  in {
    legacyPackages.${system} = pkgs;
    devShells.${system}.default = with pkgs;
      mkShellNoCC {
        name = "ros-shell";
        packages = with rosPackages.noetic; [
          roslaunch
          rosserial-arduino

          arduino-cli
          asciidoctor-with-extensions
        ];
      };
  };
}
