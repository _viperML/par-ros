// node1.ino
#include <ros.h>
#include <std_msgs/UInt16.h>

#include <Servo.h>

// Definición del servo
Servo servo;
const auto SERVO_PIN = 9;

/*
  Función de callback de subscripción.
  Se correrá una vez por cada mensaje que se reciba
*/
void servo_callback(const std_msgs::UInt16& message){
  // Mover el servo según la lectura del tópico
  servo.write(message.data);
}

// Definición del subscriptor al tópico
ros::Subscriber<std_msgs::UInt16> subscriber("light", servo_callback);

// Inicialización del nodo
ros::NodeHandle node_handle;
void setup(){
  node_handle.initNode();
  node_handle.subscribe(subscriber);

  // Inicialización del servo
  servo.attach(SERVO_PIN);
}

void loop(){
  // Ejecutar las rutinas de ROS continuamente
  node_handle.spinOnce();
  delay(1);
}
